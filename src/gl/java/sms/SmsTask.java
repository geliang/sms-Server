package gl.java.sms;

import gl.java.sms.bean.SmsBean;
import gl.java.sms.util.Result;
import gl.java.sms.util.TextUtils;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import org.apache.log4j.Logger;

import com.google.gson.JsonObject;
/**
 * 短信机通过这个接口索取短信
 * @author Administrator
 *
 */
@Path("/getsmstask")
public class SmsTask {

	@GET
	public String getSmsTask(@QueryParam("token") String token) {
		if (TextUtils.isEmpty(token)||!SMS.isLogin(token)) {
			return Result.formatErrJson("Invalid Token,must login at first");
		}
		SmsBean mSmsBean = SMS.getAWaitSms();
		if (mSmsBean != null) {
			Logger.getLogger("SmsTask").info(
					"be get an sms task :" + mSmsBean.getTel());
			JsonObject msg_content = new JsonObject();
			msg_content.addProperty("num", mSmsBean.getTel());
			msg_content.addProperty("content", mSmsBean.getContent());
			msg_content.addProperty("id", mSmsBean.getId());
			return Result.formatJson(true, msg_content.toString(), SMS.getCountOfWaitingSend()+"");
		} else {
			return Result.formatErrJson("there have not a task");
		}

	}
}