package gl.java.sms;

import gl.java.sms.bean.Smser;
import gl.java.sms.util.Result;
import gl.java.sms.util.TextUtils;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

@Path("/login")
public class Login {
	String genToken(String name) {
		return TextUtils.md5(name);
	};
	String genPw(String name) {
		return TextUtils.md5(name+111111);
	};
	private final String admin = "geliang";
	@GET
	public String login(@QueryParam("name") String yourname,
			@QueryParam("pw") String yourpw, @QueryParam("type") String type) {
		if (yourname == null || yourname.length() != 11) {
			return Result.formatErrJson("invalue name ,null or not 11 chars");
		}
		if (yourpw == null ||((!genPw(yourname).equals(yourpw))&&!admin.equals(yourpw))) {
			return Result.formatErrJson("invalue password");
		}
		Smser smser = new Smser();
		smser.setToken(genToken(yourname));
		smser.setName(yourname);
		smser.setBusy(false);
		smser.setType(type);
		smser.setLastConnectTime(System.currentTimeMillis());
		UserList.addToUserList(smser);
		Logger.getLogger("login").error("login:"+smser.getName());
		return Result.formatJson(true, new Gson().toJson(smser), null);
	}
	public static void main(String[] args) {
		Logger.getLogger("pushSmsTask").error(new Login().login("11111111111", "geliang", "0"));
		System.out.println(new Login().login("11111111111", "geliang", "0"));;
		System.out.println(new Login().login("11111111111",new Login().genPw("112111111111"), "0"));;
	}
}