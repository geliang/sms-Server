package gl.java.sms;

import gl.java.sms.bean.Smser;
import gl.java.sms.util.Result;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;


@Path("/logout")
public class Logout {

	@GET
	public String logout(@QueryParam("token") String token) {
		if (token == null ) {
			return Result.formatErrJson("invalue token ");
		}
		Smser user = UserList.getUser(token);
		if (user==null) {
			return Result.formatErrJson("invalue token ,user not login");
		}
		UserList.removeCromUserList(user);
		return Result.formatJson(true, "logout success", null);
	}
}