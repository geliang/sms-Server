package gl.java.sms.bean;

public class Smser {
	private String token;
	private String name;
	private String type;
	private boolean isBusy;
	private long lastConnectTime = 0l;
	public boolean isBusy() {
		return isBusy;
	}
	public long getLastConnectTime() {
		return lastConnectTime;
	}
	public void setLastConnectTime(long lastConnectTime) {
		this.lastConnectTime = lastConnectTime;
	}
	public void setBusy(boolean isBusy) {
		this.isBusy = isBusy;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
