/*    */package gl.java.upload;

/*    */
/*    */import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*    */
/*    */
/*    */
/*    */
/*    */

/*    */
/*    */public class BaseServlet extends HttpServlet
/*    */{
	/*    */private static final long serialVersionUID = 1L;

	/*    */
	/*    */public void doGet(HttpServletRequest request,
			HttpServletResponse response)
	/*    */throws ServletException, IOException
	/*    */{
		/* 16 */doPost(request, response);
		/*    */}

	/*    */
	/*    */public void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	/*    */{
		/* 21 */response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		/*    */}

	/*    */
	private PrintWriter mPrintWriter;

	protected void writeResponse(HttpServletResponse mHttpServletResponse,
			String resultString) {
		try {
			if (mPrintWriter == null) {
				mPrintWriter = mHttpServletResponse.getWriter();
			}
			mPrintWriter.append(resultString);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param mHttpServletResponse
	 * @param resultString
	 * @param nodes
	 *            是否加密
	 */
	protected void writeResponse(HttpServletResponse mHttpServletResponse,
			String resultString, boolean nodes) {
		try {
			if (mPrintWriter == null) {
				mPrintWriter = mHttpServletResponse.getWriter();
			}
			if (!nodes) {
				mPrintWriter.append(resultString);
			} else {
				mPrintWriter.append(resultString);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void sendResponse(HttpServletResponse mHttpServletResponse) {
		if (mPrintWriter != null) {
			mPrintWriter.flush();
			mPrintWriter.close();
			mPrintWriter = null;
		}
	}

}

/*
 * Location: C:\re\GL\WEB\TV\SERVLET\ Qualified Name:
 * org.gl.web.tv.servlet.BaseServlet JD-Core Version: 0.6.0
 */