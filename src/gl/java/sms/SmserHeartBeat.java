package gl.java.sms;

import gl.java.sms.bean.Smser;
import gl.java.sms.util.Result;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
/**
 * 短信机持续在线必须发送心跳
 * @author Administrator
 *
 */
@Path("/heartbeat")
public class SmserHeartBeat {

	@GET
	public String heartbeat(@QueryParam("token") String token) {
		if (token == null ) {
			return Result.formatErrJson("invalue token ");
		}
		Smser user = UserList.getUser(token);
		if (user==null) {
			return Result.formatErrJson("invalue token ,user not login");
		}
		user.setLastConnectTime(System.currentTimeMillis());
		return Result.formatJson(true, "heartbeat", null);
	}
}