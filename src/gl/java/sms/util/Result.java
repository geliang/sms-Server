package gl.java.sms.util;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class Result {
	public static String formatErrJson(Object msg) {
		JSONObject jo = new JSONObject();
		try {
			jo.put("result", false);

			jo.put("msg", msg);
			jo.put("type", "error");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jo.toString();
	}
	public static String formatNoticationJson(Object msg) {
		JSONObject jo = new JSONObject();
		try {
			jo.put("result", true);
			jo.put("type", "notication");
			jo.put("msg", msg);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jo.toString();
	}
	public static String formatJson(boolean isok,Object data,String msg) {
		JSONObject jo = new JSONObject();
		try {
			jo.put("result", isok);
			if (TextUtils.isEmpty(msg)) {
				msg = "";
			}
			if (data==null||TextUtils.isEmpty(data.toString())) {
				data = "";
			}
			jo.put("msg", msg);
			jo.put("data", data);
			jo.put("type", "response");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jo.toString();
	}
}
