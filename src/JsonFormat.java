import gl.java.sms.util.Result;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.google.gson.JsonObject;

@Path("/jsonformat")
public class JsonFormat {

	public static void main(String[] args) throws JSONException {
		System.out.println(new JsonFormat().response());
	}

	@GET
	@Path("/login/request")
	public String request() {
		JsonObject login = new JsonObject();
		login.addProperty("action", "login");
		login.addProperty("sn", "88888888");
		login.addProperty("username", "jlkj1");
		login.addProperty("password", "ACA458744AB11ADDF00BE");
		return login.toString();
	}

	@GET
	@Path("/login/response")
	public String response() throws JSONException {
		JSONObject login = new JSONObject();
		login.put("action", "login");
		login.put("token", "EFB89200ASACA458744AB11ADDF00BEFAABCDF190D0A");
		return gl.java.sms.util.Result.formatJson(true, login, "");
	}

	@GET
	@Path("/login/response_fail")
	public String responsefail() throws JSONException {
		JSONObject login = new JSONObject();
		login.put("action", "login");
		login.put("token", "EFB89200ASACA458744AB11ADDF00BEFAABCDF190D0A");
		return gl.java.sms.util.Result.formatErrJson("password is invalue");
	}

	@GET
	@Path("/init/request")
	public String initrequest() {
		JsonObject login = new JsonObject();
		login.addProperty("action", "init");
		login.addProperty("token",
				"EFB89200ASACA458744AB11ADDF00BEFAABCDF190D0A");
		return login.toString();
	}

	@GET
	@Path("/init/response")
	public String initresponse() throws JSONException {
		JSONObject login = new JSONObject();
		login.put("sn", "88888888");
		login.put("action", "init");
		JSONArray ja = new JSONArray();
		for (int i = 0; i < 10; i++) {
			// S_AreaManager (i_AMHID,s_AName,tmp1,tmp2,tmp3,tmp4)
			JSONObject table = new JSONObject();
			table.put("name ", "table_S_AreaManager" + i);
			JSONArray ja_data = new JSONArray();
			for (int j = 0; j < 5; j++) {
				if (j==0) {
					JSONObject data = new JSONObject();
					data.put("name", "i_AMHID" );
					data.put("value", "000231");
					ja_data.put(data);
					continue;
				}
				JSONObject data = new JSONObject();
				data.put("name", "key=" + i + ":" + j);
				data.put("value", "value=" + i + ":" + j);
				ja_data.put(data);
			}
			table.put("data ", ja_data);
			ja.put(table);
		}
		login.put("data", ja);
		return gl.java.sms.util.Result.formatJson(true, login, "");
	}

	@GET
	@Path("/init/response_fail")
	public String initresponsefail() throws JSONException {
		JSONObject login = new JSONObject();
		login.put("token", "EFB89200ASACA458744AB11ADDF00BEFAABCDF190D0A");
		login.put("action", "init");
		return gl.java.sms.util.Result.formatErrJson("password is invalue");
	}

	@GET
	@Path("/notication")
	public String notication() throws JSONException {
		JSONObject msg = new JSONObject();
		msg.put("title", "noticationtitle");
		JSONObject data = new JSONObject();
		data.put("action", "onMachineStateChanged");
		data.put("MID", "123");
		data.put("stats", "on");
		data.put("screenshotPicurl", "http://Json.org/logo.jpg");
		data.put("video", "rtsp://Json.org/live.sdp");
		msg.put("data", data);
		msg.put(".", ".");
		msg.put("..", "..");
		msg.put("...", "...");
		return Result.formatNoticationJson(msg);
	}
}
